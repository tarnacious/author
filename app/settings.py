DATABASE_URI = 'sqlite:////tmp/blog.db'
SECRET_KEY = 'development key'
DEBUG = True
PORT = 5000

TWITTER_CONSUMER_KEY = ''
TWITTER_CONSUMER_SECRET = ''

GOOGLE_CLIENT_ID = ''
GOOGLE_CLIENT_SECRET = ''
GOOGLE_REDIRECT_URI = '/auth/'

LOG_FILE = 'var/log/author.log'
LOG_FORMAT = '%(asctime)s %(levelname)s: %(message)s'

PERSONA_URL = 'http://localhost:5000'
